﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FinalPoker.Models
{
    public class Jugador
    {
        public string nombre { get; set; }
        public List<Carta> cartas { get; set; }
        public string clasificacionMano { get; set; }
        public string resultado { get; set; }
    }
}
