﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using FinalPoker.Models;
using FinalPoker.Repository;

namespace FinalPoker.Controllers
{
    public class HomeController : Controller
    {
        private List<Carta> maso;
        private readonly ISimulacion simulacion;
        public HomeController(ISimulacion _simulacion)
        {
            this.simulacion = _simulacion;
        }
        public IActionResult Index()
        {
            return View(new List<Jugador>());
        }
        [HttpPost]
        public IActionResult Simulacion(List<Jugador> jugadores)
        {
            obtenerMaso();
            var model = simulacion.RepartirCartas(jugadores, maso);
            DeterminarMano(model);
            DeterminarGanador(model);
            return View(model);
        }
        public void DeterminarGanador(List<Jugador> jugadores)
        {
            int escaleraColor = 0;
            int poker = 0;
            int full = 0;
            int color = 0;
            int escalera = 0;
            int trio = 0;
            int doblePar = 0;
            int par = 0;
            int cartaAlta = 0;

            foreach (var item in jugadores)
            {
                if (item.clasificacionMano == "EscaleraColor")
                {
                    escaleraColor++;
                }
                if (item.clasificacionMano == "Poker")
                {
                    poker++;
                }
                if (item.clasificacionMano == "Full")
                {
                    full++;
                }
                if (item.clasificacionMano == "Color")
                {
                    color++;
                }
                if (item.clasificacionMano == "Escalera")
                {
                    escalera++;
                }
                if (item.clasificacionMano == "Trio")
                {
                    trio++;
                }
                if (item.clasificacionMano == "DoblePar")
                {
                    
                    doblePar++;
                }
                if (item.clasificacionMano == "Par")
                {
                    par++;
                }
                if (item.clasificacionMano == "CartaAlta")
                {
                    cartaAlta++;
                }
            }
            if (escaleraColor > 0)
            {
                if (escaleraColor>1)
                {
                    foreach (var item in jugadores)
                    {
                        if (item.clasificacionMano == "EscaleraColor")
                        {
                            item.resultado = "Empate";
                        }
                    }
                }
                else
                {
                    jugadores.Find(o => o.clasificacionMano == "EscaleraColor").resultado = "Ganador";
                }
                return;
            }
            if (poker > 0)
            {
                if (poker > 1)
                {
                    foreach (var item in jugadores)
                    {
                        if (item.clasificacionMano == "Poker")
                        {
                            item.resultado = "Empate";
                        }
                    }
                }
                else
                {
                    jugadores.Find(o=>o.clasificacionMano=="Poker").resultado="Ganador";
                }
                return;
            }
            if (full > 0)
            {
                if (full > 1)
                {
                    foreach (var item in jugadores)
                    {
                        if (item.clasificacionMano == "Full")
                        {
                            item.resultado = "Empate";
                        }
                    }
                }
                else
                {
                    jugadores.Find(o => o.clasificacionMano == "Full").resultado = "Ganador";
                }
                return;
            }
            if (color > 0)
            {
                if (color > 1)
                {
                    foreach (var item in jugadores)
                    {
                        if (item.clasificacionMano == "Color")
                        {
                            item.resultado = "Empate";
                        }
                    }
                }
                else
                {
                    jugadores.Find(o => o.clasificacionMano == "Color").resultado = "Ganador";
                }
                return;
            }
            if (escalera > 0)
            {
                if (escalera > 1)
                {
                    foreach (var item in jugadores)
                    {
                        if (item.clasificacionMano == "Escalera")
                        {
                            item.resultado = "Empate";
                        }
                    }
                }
                else
                {
                    jugadores.Find(o => o.clasificacionMano == "Escalera").resultado = "Ganador";
                }
                return;
            }
            if (trio > 0)
            {
                if (trio > 1)
                {
                    foreach (var item in jugadores)
                    {
                        if (item.clasificacionMano == "Trio")
                        {
                            item.resultado = "Empate";
                        }
                    }
                }
                else
                {
                    jugadores.Find(o => o.clasificacionMano == "Trio").resultado = "Ganador";
                }
                return;
            }
            if (doblePar > 0)
            {
                if (doblePar > 1)
                {
                    foreach (var item in jugadores)
                    {
                        if (item.clasificacionMano == "DoblePar")
                        {
                            item.resultado = "Empate";
                        }
                    }
                }
                else
                {
                    jugadores.Find(o => o.clasificacionMano == "DoblePar").resultado = "Ganador";
                }
                return;
            }
            if (par > 0)
            {
                if (par > 1)
                {
                    foreach (var item in jugadores)
                    {
                        if (item.clasificacionMano == "Par")
                        {
                            item.resultado = "Empate";
                        }
                    }
                }
                else
                {
                    jugadores.Find(o => o.clasificacionMano == "Par").resultado = "Ganador";
                }
                return;
            }
            if (cartaAlta > 0)
            {
                if (cartaAlta > 1)
                {
                    foreach (var item in jugadores)
                    {
                        if (item.clasificacionMano == "CartaAlta")
                        {
                            item.resultado = "Empate";
                        }
                    }
                }
                return;
            }
        }
        public void DeterminarMano(List<Jugador> jugadores)
        {
            foreach (var item in jugadores)
            {
                if (Par(item.cartas))
                {
                    item.clasificacionMano = "Par";
                }
                if (DoblePar(item.cartas))
                {
                    item.clasificacionMano = "DoblePar";
                }
                if (Trio(item.cartas))
                {
                    item.clasificacionMano = "Trio";
                }
                if (Escalera(item.cartas))
                {
                    item.clasificacionMano = "Escalera";
                }
                if (Color(item.cartas))
                {
                    item.clasificacionMano = "Color";
                }
                if (Full(item.cartas))
                {
                    item.clasificacionMano = "Full";
                }
                if (Poker(item.cartas))
                {
                    item.clasificacionMano = "Poker";
                }
                if (EscaleraColor(item.cartas))
                {
                    item.clasificacionMano = "EscaleraColor";
                }
                if (item.clasificacionMano==null)
                {
                    item.clasificacionMano = "CartaAlta";
                }
            }
        }
        public bool EscaleraColor(List<Carta> cartas)
        {
            cartas = cartas.OrderByDescending(o => o.numero).ToList();
            int t = 0;
            int d=0;
            bool resultado= true;
            foreach (var item in cartas)
            {
                if (t-1==item.numero && d==item.palo)
                {
                    t = item.numero ;
                    resultado = true;
                }
                else
                {
                    resultado = false;
                }
                if (t==0)
                {
                    t=item.numero;
                    d = item.palo;
                }
            }
            return resultado ;
        }
        public bool Poker(List<Carta> cartas)
        {
            cartas = cartas.OrderByDescending(o => o.numero).ToList();
            int cont=0;
            int val = 0;
            bool resultado = false;
            foreach (var item in cartas)
            {
                
                if (val == item.numero)
                {
                    cont++;
                    if (cont == 4)
                    {
                        return resultado = true;
                    }
                }
                else
                {
                    cont = 0;
                }
                if (val == 0)
                {
                    val = item.numero;
                    cont++;
                }
            }
            return resultado;
        }
        public bool Full(List<Carta> cartas)
        {
            cartas = cartas.OrderByDescending(o => o.numero).ToList();
            int cont1 = 0;
            int cont2 = 0;
            int val = 0;
            int val2=0;
            bool resultado = false;
            foreach (var item in cartas)
            {
                if (val == item.numero)
                {
                    cont1++;
                }
                if (cont1==0)
                {
                    val = item.numero;
                    cont1++;
                }
                if (cont1>=2)
                {
                    if (val2==item.numero)
                    {
                        cont2++;
                    }
                    else
                    {
                        val2 = item.numero;
                        cont2 = 1;
                    }
                    if (cont2==0)
                    {
                        val2 = item.numero;
                        cont2++;
                    }
                }
            }
            if (cont1==3 && cont2==2)
            {
                resultado = true;
            }else if (cont1 == 2 && cont2 == 3)
            {
                resultado = true;
            }
            return resultado;
        }
        public bool Color(List<Carta> cartas)
        {
            cartas = cartas.OrderByDescending(o => o.numero).ToList();
            int cont = 0;
            int palo = 0;
            bool resultado = true;
            foreach (var item in cartas)
            {
                if (palo==item.palo)
                {
                    cont++;
                }
                if (cont==0)
                {
                    palo = item.palo;
                    cont++;
                }
                if (cont<5)
                {
                    resultado = false;
                }
                else
                {
                    resultado = true;
                }
            }
            return resultado;
        }
        public bool Escalera(List<Carta> cartas)
        {
            cartas = cartas.OrderByDescending(o => o.numero).ToList();
            int cont = 0;
            int carta = 0;
            bool resultado = true;
            foreach (var item in cartas)
            {
                if (carta-1 == item.numero)
                {
                    carta = item.numero;
                    cont++;
                }
                else if(cont>0)
                {
                    carta = item.numero;
                }
                if (item.numero==14)
                {
                    cont++;
                }
                if (carta == 0)
                {
                    carta = item.numero;
                    cont++;
                }
                if (cont < 5)
                {
                    resultado = false;
                }
                else
                {
                    resultado = true;
                }
            }
            return resultado;
        }
        public bool Trio(List<Carta> cartas)
        {
            cartas = cartas.OrderByDescending(o => o.numero).ToList();
            int cont = 0;
            int val = 0;
            bool resultado = false;
            foreach (var item in cartas)
            {
                if (val == item.numero)
                {
                    val = item.numero;
                    cont++;
                }
                else
                {
                    val = item.numero;
                    cont = 1;
                }
                if (val == 0)
                {
                    val = item.numero;
                    cont++;
                }
                if (cont==3)
                {
                    return true;
                }
            }
            return resultado;
        }
        public bool DoblePar(List<Carta> cartas)
        {
            cartas = cartas.OrderByDescending(o => o.numero).ToList();
            int cont1 = 0;
            int cont2 = 0;
            int val = 0;
            int val2 = 0;
            bool resultado = false;
            foreach (var item in cartas)
            {
                if (val == item.numero)
                {
                    cont1++;
                }
                if (cont1 == 0)
                {
                    val = item.numero;
                    cont1++;
                }
                if (cont1 == 2 && cont2<2)
                {
                    if (val2 == item.numero)
                    {
                        cont2++;
                    }
                    else
                    {
                        val2 = item.numero;
                        cont2 = 1;
                    }
                    if (cont2 == 0)
                    {
                        val2 = item.numero;
                        cont2++;
                    }
                }
            }
            if (cont1 == 2 && cont2 == 2)
            {
                resultado = true;
            }
            else if (cont1 == 2 && cont2 == 2)
            {
                resultado = true;
            }
            return resultado;
        }
        public bool Par(List<Carta> cartas)
        {
            cartas = cartas.OrderByDescending(o => o.numero).ToList();
            int cont = 0;
            int val = 0;
            bool resultado = false;
            foreach (var item in cartas)
            {
                if (val == item.numero)
                {
                    val = item.numero;
                    cont++;
                }
                else
                {
                    val = item.numero;
                    cont = 1;
                }
                if (val == 0)
                {
                    val = item.numero;
                    cont++;
                }
                if (cont == 2)
                {
                    return true;
                }
            }
            return resultado;
        }
        public void obtenerMaso()
        {
            string cartas = "[" +
                                "{\"numero\":14,\"palo\":1}," +
                                "{\"numero\":2,\"palo\":1}," +
                                "{\"numero\":3,\"palo\":1}," +
                                "{\"numero\":4,\"palo\":1}," +
                                "{\"numero\":5,\"palo\":1}," +
                                "{\"numero\":6,\"palo\":1}," +
                                "{\"numero\":7,\"palo\":1}," +
                                "{\"numero\":8,\"palo\":1}," +
                                "{\"numero\":9,\"palo\":1}," +
                                "{\"numero\":10,\"palo\":1}," +
                                "{\"numero\":11,\"palo\":1}," +
                                "{\"numero\":12,\"palo\":1}," +
                                "{\"numero\":13,\"palo\":1}," +   ///
                                "{\"numero\":14,\"palo\":2}," +
                                "{\"numero\":2,\"palo\":2}," +
                                "{\"numero\":3,\"palo\":2}," +
                                "{\"numero\":4,\"palo\":2}," +
                                "{\"numero\":5,\"palo\":2}," +
                                "{\"numero\":6,\"palo\":2}," +
                                "{\"numero\":7,\"palo\":2}," +
                                "{\"numero\":8,\"palo\":2}," +
                                "{\"numero\":9,\"palo\":2}," +
                                "{\"numero\":10,\"palo\":2}," +
                                "{\"numero\":11,\"palo\":2}," +
                                "{\"numero\":12,\"palo\":2}," +
                                "{\"numero\":13,\"palo\":2}," +///
                                "{\"numero\":14,\"palo\":3}," +
                                "{\"numero\":2,\"palo\":3}," +
                                "{\"numero\":3,\"palo\":3}," +
                                "{\"numero\":4,\"palo\":3}," +
                                "{\"numero\":5,\"palo\":3}," +
                                "{\"numero\":6,\"palo\":3}," +
                                "{\"numero\":7,\"palo\":3}," +
                                "{\"numero\":8,\"palo\":3}," +
                                "{\"numero\":9,\"palo\":3}," +
                                "{\"numero\":10,\"palo\":3}," +
                                "{\"numero\":11,\"palo\":3}," +
                                "{\"numero\":12,\"palo\":3}," +
                                "{\"numero\":13,\"palo\":3}," +///
                                "{\"numero\":14,\"palo\":4}," +
                                "{\"numero\":2,\"palo\":4}," +
                                "{\"numero\":3,\"palo\":4}," +
                                "{\"numero\":4,\"palo\":4}," +
                                "{\"numero\":5,\"palo\":4}," +
                                "{\"numero\":6,\"palo\":4}," +
                                "{\"numero\":7,\"palo\":4}," +
                                "{\"numero\":8,\"palo\":4}," +
                                "{\"numero\":9,\"palo\":4}," +
                                "{\"numero\":10,\"palo\":4}," +
                                "{\"numero\":11,\"palo\":4}," +
                                "{\"numero\":12,\"palo\":4}," +
                                "{\"numero\":13,\"palo\":4}" +
                                "]";
            maso = System.Text.Json.JsonSerializer.Deserialize<List<Carta>>(cartas);
        }
    }
}
