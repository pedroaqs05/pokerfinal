﻿using FinalPoker.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FinalPoker.Repository
{
    public interface ISimulacion
    {
        public List<Jugador> RepartirCartas(List<Jugador> jugadores, List<Carta> maso);
    }
    public class Simulacion : ISimulacion
    {
        private Random random = new Random();
        public List<Jugador> RepartirCartas(List<Jugador> jugadores, List<Carta> maso)
        {
            foreach (var jugador in jugadores)
            {
                List<Carta> cartas = new List<Carta>();
                int nc = 0;
                while (nc < 5)
                {
                    var index = random.Next(maso.Count());
                    var carta = maso.ElementAt(index);
                    maso.RemoveAt(index);
                    nc++;
                    cartas.Add(carta);
                }
                cartas = cartas.OrderByDescending(o => o.numero).ToList();
                jugador.cartas = cartas;
            }
            return jugadores;
        }

    }
}
