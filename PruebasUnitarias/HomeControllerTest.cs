﻿using FinalPoker.Controllers;
using FinalPoker.Models;
using FinalPoker.Repository;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace PruebasUnitarias
{
    class HomeControllerTest
    {
        [Test]
        public void DeterminarEscaleraColor()
        {
            var listaCartas = new List<Carta>();
            var carta1 = new Carta();
            carta1.numero = 13;
            carta1.palo = 1;
            listaCartas.Add(carta1);
            var carta2 = new Carta();
            carta2.numero = 12;
            carta2.palo = 1;
            listaCartas.Add(carta2);
            var carta3 = new Carta();
            carta3.numero = 11;
            carta3.palo = 1;
            listaCartas.Add(carta3);
            var carta4 = new Carta();
            carta4.numero = 10;
            carta4.palo = 1;
            listaCartas.Add(carta4);
            var carta5 = new Carta();
            carta5.numero = 9;
            carta5.palo = 1;
            listaCartas.Add(carta5);
            var simulacion = new Mock<ISimulacion>();
            var controller = new HomeController(simulacion.Object);
            var result = controller.EscaleraColor(listaCartas);
            Assert.IsTrue(result);
        }
        [Test]
        public void DeterminarManoNoEsEscaleraColor()
        {
            var listaCartas = new List<Carta>();
            var carta1 = new Carta();
            carta1.numero = 13;
            carta1.palo = 1;
            listaCartas.Add(carta1);
            var carta2 = new Carta();
            carta2.numero = 12;
            carta2.palo = 1;
            listaCartas.Add(carta2);
            var carta3 = new Carta();
            carta3.numero = 11;
            carta3.palo = 1;
            listaCartas.Add(carta3);
            var carta4 = new Carta();
            carta4.numero = 10;
            carta4.palo = 2;
            listaCartas.Add(carta4);
            var carta5 = new Carta();
            carta5.numero = 9;
            carta5.palo = 1;
            listaCartas.Add(carta5);
            var simulacion = new Mock<ISimulacion>();
            var controller = new HomeController(simulacion.Object);
            var result = controller.EscaleraColor(listaCartas);
            Assert.IsFalse(result);
        }
        [Test]
        public void DeterminarPoker()
        {
            var listaCartas = new List<Carta>();
            var carta1 = new Carta();
            carta1.numero = 13;
            carta1.palo = 1;
            listaCartas.Add(carta1);
            var carta2 = new Carta();
            carta2.numero = 13;
            carta2.palo = 2;
            listaCartas.Add(carta2);
            var carta3 = new Carta();
            carta3.numero = 13;
            carta3.palo = 3;
            listaCartas.Add(carta3);
            var carta4 = new Carta();
            carta4.numero = 13;
            carta4.palo = 4;
            listaCartas.Add(carta4);
            var carta5 = new Carta();
            carta5.numero = 9;
            carta5.palo = 1;
            listaCartas.Add(carta5);
            var simulacion = new Mock<ISimulacion>();
            var controller = new HomeController(simulacion.Object);
            var result = controller.Poker(listaCartas);
            Assert.IsTrue(result);
        }
        [Test]
        public void DeterminarFULL()
        {
            var listaCartas = new List<Carta>();
            var carta1 = new Carta();
            carta1.numero = 13;
            carta1.palo = 1;
            listaCartas.Add(carta1);
            var carta2 = new Carta();
            carta2.numero = 13;
            carta2.palo = 2;
            listaCartas.Add(carta2);
            var carta3 = new Carta();
            carta3.numero = 13;
            carta3.palo = 3;
            listaCartas.Add(carta3);
            var carta4 = new Carta();
            carta4.numero = 9;
            carta4.palo = 4;
            listaCartas.Add(carta4);
            var carta5 = new Carta();
            carta5.numero = 9;
            carta5.palo = 1;
            listaCartas.Add(carta5);
            var simulacion = new Mock<ISimulacion>();
            var controller = new HomeController(simulacion.Object);
            var result = controller.Full(listaCartas);
            Assert.IsTrue(result);
        }
        [Test]
        public void DeterminarManoNoEsFULL()
        {
            var listaCartas = new List<Carta>();
            var carta1 = new Carta();
            carta1.numero = 13;
            carta1.palo = 1;
            listaCartas.Add(carta1);
            var carta2 = new Carta();
            carta2.numero = 13;
            carta2.palo = 2;
            listaCartas.Add(carta2);
            var carta3 = new Carta();
            carta3.numero = 13;
            carta3.palo = 3;
            listaCartas.Add(carta3);
            var carta4 = new Carta();
            carta4.numero = 9;
            carta4.palo = 4;
            listaCartas.Add(carta4);
            var carta5 = new Carta();
            carta5.numero = 8;
            carta5.palo = 1;
            listaCartas.Add(carta5);
            var simulacion = new Mock<ISimulacion>();
            var controller = new HomeController(simulacion.Object);
            var result = controller.Full(listaCartas);
            Assert.IsFalse(result);
        }
        [Test]
        public void DeterminarFULLTresFinal()
        {
            var listaCartas = new List<Carta>();
            var carta1 = new Carta();
            carta1.numero = 9;
            carta1.palo = 1;
            listaCartas.Add(carta1);
            var carta2 = new Carta();
            carta2.numero = 9;
            carta2.palo = 2;
            listaCartas.Add(carta2);
            var carta3 = new Carta();
            carta3.numero = 13;
            carta3.palo = 3;
            listaCartas.Add(carta3);
            var carta4 = new Carta();
            carta4.numero = 13;
            carta4.palo = 4;
            listaCartas.Add(carta4);
            var carta5 = new Carta();
            carta5.numero = 13;
            carta5.palo = 1;
            listaCartas.Add(carta5);
            var simulacion = new Mock<ISimulacion>();
            var controller = new HomeController(simulacion.Object);
            var result = controller.Full(listaCartas);
            Assert.IsTrue(result);
        }
        [Test]
        public void DeterminarColor()
        {
            var listaCartas = new List<Carta>();
            var carta1 = new Carta();
            carta1.numero = 10;
            carta1.palo = 1;
            listaCartas.Add(carta1);
            var carta2 = new Carta();
            carta2.numero = 13;
            carta2.palo = 1;
            listaCartas.Add(carta2);
            var carta3 = new Carta();
            carta3.numero = 12;
            carta3.palo = 1;
            listaCartas.Add(carta3);
            var carta4 = new Carta();
            carta4.numero = 9;
            carta4.palo = 1;
            listaCartas.Add(carta4);
            var carta5 = new Carta();
            carta5.numero = 2;
            carta5.palo = 1;
            listaCartas.Add(carta5);
            var simulacion = new Mock<ISimulacion>();
            var controller = new HomeController(simulacion.Object);
            var result = controller.Color(listaCartas);
            Assert.IsTrue(result);
        }
        [Test]
        public void DeterminarManoNoEsColor()
        {
            var listaCartas = new List<Carta>();
            var carta1 = new Carta();
            carta1.numero = 10;
            carta1.palo = 1;
            listaCartas.Add(carta1);
            var carta2 = new Carta();
            carta2.numero = 13;
            carta2.palo = 1;
            listaCartas.Add(carta2);
            var carta3 = new Carta();
            carta3.numero = 12;
            carta3.palo = 1;
            listaCartas.Add(carta3);
            var carta4 = new Carta();
            carta4.numero = 9;
            carta4.palo = 2;
            listaCartas.Add(carta4);
            var carta5 = new Carta();
            carta5.numero = 2;
            carta5.palo = 1;
            listaCartas.Add(carta5);
            var simulacion = new Mock<ISimulacion>();
            var controller = new HomeController(simulacion.Object);
            var result = controller.Color(listaCartas);
            Assert.IsFalse(result);
        }
        [Test]
        public void DeterminarEscaleraNormal()
        {
            var listaCartas = new List<Carta>();
            var carta1 = new Carta();
            carta1.numero = 10;
            carta1.palo = 2;
            listaCartas.Add(carta1);
            var carta2 = new Carta();
            carta2.numero = 9;
            carta2.palo = 3;
            listaCartas.Add(carta2);
            var carta3 = new Carta();
            carta3.numero = 8;
            carta3.palo = 1;
            listaCartas.Add(carta3);
            var carta4 = new Carta();
            carta4.numero = 7;
            carta4.palo = 1;
            listaCartas.Add(carta4);
            var carta5 = new Carta();
            carta5.numero = 6;
            carta5.palo = 4;
            listaCartas.Add(carta5);
            var simulacion = new Mock<ISimulacion>();
            var controller = new HomeController(simulacion.Object);
            var result = controller.Escalera(listaCartas);
            Assert.IsTrue(result);
        }
        [Test]
        public void DeterminarEscaleraNormalDesorden()
        {
            var listaCartas = new List<Carta>();
            var carta1 = new Carta();
            carta1.numero = 10;
            carta1.palo = 2;
            listaCartas.Add(carta1);
            var carta2 = new Carta();
            carta2.numero = 13;
            carta2.palo = 3;
            listaCartas.Add(carta2);
            var carta3 = new Carta();
            carta3.numero = 9;
            carta3.palo = 1;
            listaCartas.Add(carta3);
            var carta4 = new Carta();
            carta4.numero = 12;
            carta4.palo = 1;
            listaCartas.Add(carta4);
            var carta5 = new Carta();
            carta5.numero = 11;
            carta5.palo = 4;
            listaCartas.Add(carta5);
            var simulacion = new Mock<ISimulacion>();
            var controller = new HomeController(simulacion.Object);
            var result = controller.Escalera(listaCartas);
            Assert.IsTrue(result);
        }
        [Test]
        public void DeterminarEscaleraConAsValorUno()
        {
            var listaCartas = new List<Carta>();
            var carta1 = new Carta();
            carta1.numero = 5;
            carta1.palo = 2;
            listaCartas.Add(carta1);
            var carta2 = new Carta();
            carta2.numero = 4;
            carta2.palo = 3;
            listaCartas.Add(carta2);
            var carta3 = new Carta();
            carta3.numero = 3;
            carta3.palo = 1;
            listaCartas.Add(carta3);
            var carta4 = new Carta();
            carta4.numero = 2;
            carta4.palo = 1;
            listaCartas.Add(carta4);
            var carta5 = new Carta();
            carta5.numero = 14;
            carta5.palo = 4;
            listaCartas.Add(carta5);
            var simulacion = new Mock<ISimulacion>();
            var controller = new HomeController(simulacion.Object);
            var result = controller.Escalera(listaCartas);
            Assert.IsTrue(result);
        }
        [Test]
        public void DeterminarTrio()
        {
            var listaCartas = new List<Carta>();
            var carta1 = new Carta();
            carta1.numero = 4;
            carta1.palo = 2;
            listaCartas.Add(carta1);
            var carta2 = new Carta();
            carta2.numero = 5;
            carta2.palo = 3;
            listaCartas.Add(carta2);
            var carta3 = new Carta();
            carta3.numero = 13;
            carta3.palo = 1;
            listaCartas.Add(carta3);
            var carta4 = new Carta();
            carta4.numero = 13;
            carta4.palo = 2;
            listaCartas.Add(carta4);
            var carta5 = new Carta();
            carta5.numero = 13;
            carta5.palo = 4;
            listaCartas.Add(carta5);
            var simulacion = new Mock<ISimulacion>();
            var controller = new HomeController(simulacion.Object);
            var result = controller.Trio(listaCartas);
            Assert.IsTrue(result);
        }
        [Test]
        public void DeterminarManoNoEsTrio()
        {
            var listaCartas = new List<Carta>();
            var carta1 = new Carta();
            carta1.numero = 4;
            carta1.palo = 2;
            listaCartas.Add(carta1);
            var carta2 = new Carta();
            carta2.numero = 5;
            carta2.palo = 3;
            listaCartas.Add(carta2);
            var carta3 = new Carta();
            carta3.numero = 5;
            carta3.palo = 1;
            listaCartas.Add(carta3);
            var carta4 = new Carta();
            carta4.numero = 13;
            carta4.palo = 2;
            listaCartas.Add(carta4);
            var carta5 = new Carta();
            carta5.numero = 13;
            carta5.palo = 4;
            listaCartas.Add(carta5);
            var simulacion = new Mock<ISimulacion>();
            var controller = new HomeController(simulacion.Object);
            var result = controller.Trio(listaCartas);
            Assert.IsFalse(result);
        }
        [Test]
        public void DeterminarDoblePar()
        {
            var listaCartas = new List<Carta>();
            var carta1 = new Carta();
            carta1.numero = 4;
            carta1.palo = 2;
            listaCartas.Add(carta1);
            var carta2 = new Carta();
            carta2.numero = 4;
            carta2.palo = 3;
            listaCartas.Add(carta2);
            var carta3 = new Carta();
            carta3.numero = 2;
            carta3.palo = 1;
            listaCartas.Add(carta3);
            var carta4 = new Carta();
            carta4.numero = 13;
            carta4.palo = 2;
            listaCartas.Add(carta4);
            var carta5 = new Carta();
            carta5.numero = 13;
            carta5.palo = 4;
            listaCartas.Add(carta5);
            var simulacion = new Mock<ISimulacion>();
            var controller = new HomeController(simulacion.Object);
            var result = controller.DoblePar(listaCartas);
            Assert.IsTrue(result);
        }
        [Test]
        public void DeterminarManoNoEsDoble()
        {
            var listaCartas = new List<Carta>();
            var carta1 = new Carta();
            carta1.numero = 4;
            carta1.palo = 2;
            listaCartas.Add(carta1);
            var carta2 = new Carta();
            carta2.numero = 4;
            carta2.palo = 3;
            listaCartas.Add(carta2);
            var carta3 = new Carta();
            carta3.numero = 2;
            carta3.palo = 1;
            listaCartas.Add(carta3);
            var carta4 = new Carta();
            carta4.numero = 13;
            carta4.palo = 2;
            listaCartas.Add(carta4);
            var carta5 = new Carta();
            carta5.numero = 14;
            carta5.palo = 4;
            listaCartas.Add(carta5);
            var simulacion = new Mock<ISimulacion>();
            var controller = new HomeController(simulacion.Object);
            var result = controller.DoblePar(listaCartas);
            Assert.IsFalse(result);
        }
        [Test]
        public void DeterminarDobleParIncorrecto()
        {
            var listaCartas = new List<Carta>();
            var carta1 = new Carta();
            carta1.numero = 12;
            carta1.palo = 3;
            listaCartas.Add(carta1);
            var carta2 = new Carta();
            carta2.numero = 12;
            carta2.palo = 1;
            listaCartas.Add(carta2);
            var carta3 = new Carta();
            carta3.numero = 8;
            carta3.palo = 3;
            listaCartas.Add(carta3);
            var carta4 = new Carta();
            carta4.numero = 7;
            carta4.palo = 2;
            listaCartas.Add(carta4);
            var carta5 = new Carta();
            carta5.numero = 2;
            carta5.palo = 4;
            listaCartas.Add(carta5);
            var simulacion = new Mock<ISimulacion>();
            var controller = new HomeController(simulacion.Object);
            var result = controller.DoblePar(listaCartas);
            Assert.IsFalse(result);
        }
        [Test]
        public void DeterminarPar()
        {
            var listaCartas = new List<Carta>();
            var carta1 = new Carta();
            carta1.numero = 8;
            carta1.palo = 3;
            listaCartas.Add(carta1);
            var carta2 = new Carta();
            carta2.numero = 12;
            carta2.palo = 1;
            listaCartas.Add(carta2);
            var carta3 = new Carta();
            carta3.numero = 12;
            carta3.palo = 3;
            listaCartas.Add(carta3);
            var carta4 = new Carta();
            carta4.numero = 7;
            carta4.palo = 2;
            listaCartas.Add(carta4);
            var carta5 = new Carta();
            carta5.numero = 2;
            carta5.palo = 4;
            listaCartas.Add(carta5);
            var simulacion = new Mock<ISimulacion>();
            var controller = new HomeController(simulacion.Object);
            var result = controller.Par(listaCartas);
            Assert.IsTrue(result);
        }
        [Test]
        public void DeterminarManoNoEsPar()
        {
            var listaCartas = new List<Carta>();
            var carta1 = new Carta();
            carta1.numero = 8;
            carta1.palo = 3;
            listaCartas.Add(carta1);
            var carta2 = new Carta();
            carta2.numero = 12;
            carta2.palo = 1;
            listaCartas.Add(carta2);
            var carta3 = new Carta();
            carta3.numero = 14;
            carta3.palo = 3;
            listaCartas.Add(carta3);
            var carta4 = new Carta();
            carta4.numero = 7;
            carta4.palo = 2;
            listaCartas.Add(carta4);
            var carta5 = new Carta();
            carta5.numero = 2;
            carta5.palo = 4;
            listaCartas.Add(carta5);
            var simulacion = new Mock<ISimulacion>();
            var controller = new HomeController(simulacion.Object);
            var result = controller.Par(listaCartas);
            Assert.IsFalse(result);
        }
        [Test]
        public void DeterminarParEnDesorden()
        {
            var listaCartas = new List<Carta>();
            var carta1 = new Carta();
            carta1.numero = 8;
            carta1.palo = 3;
            listaCartas.Add(carta1);
            var carta2 = new Carta();
            carta2.numero = 12;
            carta2.palo = 1;
            listaCartas.Add(carta2);
            var carta3 = new Carta();
            carta3.numero = 2;
            carta3.palo = 3;
            listaCartas.Add(carta3);
            var carta4 = new Carta();
            carta4.numero = 7;
            carta4.palo = 2;
            listaCartas.Add(carta4);
            var carta5 = new Carta();
            carta5.numero = 12;
            carta5.palo = 4;
            listaCartas.Add(carta5);
            var simulacion = new Mock<ISimulacion>();
            var controller = new HomeController(simulacion.Object);
            var result = controller.Par(listaCartas);
            Assert.IsTrue(result);
        }
        [Test]
        public void DeterminarJugadorGanador()
        {
            List<Jugador> jugadores = new List<Jugador>();
            Jugador jugador1 = new Jugador();
            List<Carta> cartas1 = new List<Carta>();
            var carta1 = new Carta();
            carta1.numero = 9;
            carta1.palo = 1;
            cartas1.Add(carta1);
            var carta2 = new Carta();
            carta2.numero = 8;
            carta2.palo = 1;
            cartas1.Add(carta2);
            var carta3 = new Carta();
            carta3.numero = 7;
            carta3.palo = 1;
            cartas1.Add(carta3);
            var carta4 = new Carta();
            carta4.numero = 6;
            carta4.palo = 1;
            cartas1.Add(carta4);
            var carta5 = new Carta();
            carta5.numero = 5;
            carta5.palo = 1;
            cartas1.Add(carta5);
            jugador1.nombre = "Pedro";
            jugador1.cartas = cartas1;
            jugadores.Add(jugador1);
            var simulacion = new Mock<ISimulacion>();
            var controller = new HomeController(simulacion.Object);
            controller.DeterminarMano(jugadores);
            controller.DeterminarGanador(jugadores);
            Assert.AreEqual("Ganador", jugadores[0].resultado);
        }
        [Test]
        public void DeterminarJugadorEmpate()
        {
            List<Jugador> jugadores = new List<Jugador>();
            Jugador jugador1 = new Jugador();
            List<Carta> cartas1 = new List<Carta>();
            var carta1 = new Carta();
            carta1.numero = 2;
            carta1.palo = 1;
            cartas1.Add(carta1);
            var carta2 = new Carta();
            carta2.numero = 2;
            carta2.palo = 2;
            cartas1.Add(carta2);
            var carta3 = new Carta();
            carta3.numero = 7;
            carta3.palo = 1;
            cartas1.Add(carta3);
            var carta4 = new Carta();
            carta4.numero = 6;
            carta4.palo = 1;
            cartas1.Add(carta4);
            var carta5 = new Carta();
            carta5.numero = 5;
            carta5.palo = 1;
            cartas1.Add(carta5);
            jugador1.nombre = "Pedro";
            jugador1.cartas = cartas1;
            jugadores.Add(jugador1);
            Jugador jugador2 = new Jugador();
            List<Carta> cartas2 = new List<Carta>();
            var carta12 = new Carta();
            carta12.numero = 14;
            carta12.palo = 1;
            cartas2.Add(carta12);
            var carta22 = new Carta();
            carta22.numero = 12;
            carta22.palo = 2;
            cartas2.Add(carta22);
            var carta32 = new Carta();
            carta32.numero = 4;
            carta32.palo = 1;
            cartas2.Add(carta32);
            var carta42 = new Carta();
            carta42.numero = 4;
            carta42.palo = 1;
            cartas2.Add(carta42);
            var carta52 = new Carta();
            carta52.numero = 5;
            carta52.palo = 1;
            cartas2.Add(carta52);
            jugador2.nombre = "Juan";
            jugador2.cartas = cartas2;
            jugadores.Add(jugador2);
            var simulacion = new Mock<ISimulacion>();
            var controller = new HomeController(simulacion.Object);
            controller.DeterminarMano(jugadores);
            controller.DeterminarGanador(jugadores);
            Assert.AreEqual("Empate", jugadores[0].resultado);
            Assert.AreEqual("Empate", jugadores[1].resultado);
        }
    }
}
